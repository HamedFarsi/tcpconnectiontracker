#pragma once
#include <QtCore/QCoreApplication>
#define MAC_TYPE_OFF							12
#define IPV4_PROTOCOL_OFF						23
#define ETH_IPV4_SRC_OFF						26
#define ETH_IPV4_DST_OFF						30
#define ETH_IPV4_HDR_OFF						14
#define IPV4_DEFAULT_HEADER_LEN				20
#define ZM_uint32_reverse(p) ((unsigned int)(((p&0xff)<<24)|((p&0xff00)<<8)|(((unsigned int)(p)&0xff0000)>>8)|(((unsigned int)(p)&0xff000000)>>24)))

class packet_fcn
{
public:
	const unsigned char * pkt;
	packet_fcn(void);
	~packet_fcn(void);
	
	inline quint32	tcp_get_ack_num()const
	{
		return ZM_uint32_reverse(*(quint32*)(pkt+ETH_IPV4_HDR_OFF+IPV4_DEFAULT_HEADER_LEN+8));
	}
	inline quint8	tcp_get_flags() const
	{
		return *((quint8 *) (pkt + ETH_IPV4_HDR_OFF+IPV4_DEFAULT_HEADER_LEN+13));
	}
	inline int	eth_get_r_type () const
	{
		quint16 rip = *((quint16 *) (pkt + MAC_TYPE_OFF));
		return rip;
	}
	inline quint8	ip_get_protocol () const
	{
		return *(pkt + IPV4_PROTOCOL_OFF);
	}
	inline quint32	ip_get_r_src () const
	{
		quint32 rip = *((quint32 *) (pkt + ETH_IPV4_SRC_OFF));
		return rip;
	}
	inline quint32	ip_get_r_dst () const
	{
		quint32 rip = *((quint32 *) (pkt + ETH_IPV4_DST_OFF));
		return rip;
	}
	inline quint16	tcp_udp_get_r_src_port() const
	{
		quint16 r = *((quint16 *) (pkt + ETH_IPV4_HDR_OFF + IPV4_DEFAULT_HEADER_LEN));
		return r;
	}
	inline quint16	tcp_udp_get_r_dst_port () const
	{
		quint16 r = *((quint16 *) (pkt + ETH_IPV4_HDR_OFF+IPV4_DEFAULT_HEADER_LEN+2));
		return r;
	}
	inline quint32	tcp_get_seq_num() const
	{
		quint32 a =ZM_uint32_reverse(*(quint32*)(pkt+ETH_IPV4_HDR_OFF+IPV4_DEFAULT_HEADER_LEN+4)) ;
		return a;
	}
};
