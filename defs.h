#pragma  once
#include <QString>
enum direction
{
	DIR				  = 0,
	REV				  = 1,
	UNKNOWN_DIRECTION = 2,
	DIRECTION_SIZE	//must be last element
};
enum pkt_type 
{
	//order is very important
	SYN			= 0,
	SYNACK		= 1,
	FIN			= 2,
	RST			= 3,
	ACK			= 4,
	PKT_TYPE_SIZE ,	//must be last element
	UNKNOWN
};
inline QString pkt_type_toString(pkt_type typ)
{
	switch(typ)
	{
	case SYN:
		return "SYN";
	case SYNACK:
		return "SYNACK";
	case FIN:
		return "FIN";
	case RST:
		return "RST" ;
	case ACK:
		return "ACK"; 
	case UNKNOWN:
		return "UNKNOWN";
	case PKT_TYPE_SIZE:
		return "PKT_TYPE_SIZE";
	}
}
enum tcp_states
{
	SYN_SENT		   = 0,
	SYN_RCV			   = 1,
	ESTABLISHED 	   = 2,
	FINISHING		   = 3,
	TIME_WAIT		   = 4,
	CLOSED			   = 5,
	TCP_STATES_SIZE	//must be last element
};
inline QString tcp_states_toString(tcp_states st)
{
	switch(st)
	{
	case SYN_SENT:
		return "SYN_SENT";
	case SYN_RCV:
		return "SYN_RCV";
	case ESTABLISHED:
		return "ESTABLISHED";
	case FINISHING:
		return "FINISHING";
	case TIME_WAIT:
		return "TIME_WAIT";
	case CLOSED:
		return "CLOSED";
	case TCP_STATES_SIZE:
		return "TCP_STATES_SIZE" ;
	}
	return "";
}

