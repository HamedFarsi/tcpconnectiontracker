#ifndef TCP_TRACKER_INTERFACE_H
#define TCP_TRACKER_INTERFACE_H

#include <QMainWindow>
#include "con_track.h"
#include <QTimer>
#define ZM_uint16_reverse(x) ( (((x) & 0xff) << 8) | ((unsigned short)(x) >> 8) )

namespace Ui {
class tcp_tracker_interface;
}

class tcp_tracker_interface : public QMainWindow
{
    Q_OBJECT

public:
    explicit tcp_tracker_interface(QWidget *parent = 0);
    ~tcp_tracker_interface();
	int find_row_number(QStringList);
	public slots:
		void del_exp(tcp_connection_selector sel);

private slots:
    void on_btn_track_clicked();
	void refresh_table();

	void set_color( QString str_state, int recored_id );
public:
    Ui::tcp_tracker_interface *ui;
    con_track _track ;
	QTimer _timer ;
};

#endif // TCP_TRACKER_INTERFACE_H
