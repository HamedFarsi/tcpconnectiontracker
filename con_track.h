#pragma once
#include <QtCore/QCoreApplication>
#include <QMutex>
#include <QReadWriteLock>
#include<queue>
#include "defs.h"
#include <time.h>
#include <map>
#include <QThread>
#include <QWriteLocker>
#include "packet_fcn.h"
#include <stdio.h>
#define HAVE_REMOTE
#include "pcap.h"
using namespace std;
#define	ETH_TYPE_R_IPV4						0x0008
struct tcp_connection_selector
{
public:
	quint32	src_ip;
	quint32 dst_ip;
	quint16 src_port;
	quint16 dst_port;
	int		protocol;
	tcp_connection_selector(){}
	tcp_connection_selector(quint32 t_src_ip,quint32 t_dst_ip, quint16 t_src_port,quint16 t_dst_port,int t_protocol)
		: src_ip(t_src_ip)
		, dst_ip(t_dst_ip)
		, src_port(t_src_port)
		, dst_port(t_dst_port)
		, protocol(t_protocol)
	{}
	bool operator < (const tcp_connection_selector& cs)const
	{
		if(src_port < cs.src_port)
			return true;
		else if(src_port > cs.src_port)
			return false;
		if(src_ip < cs.src_ip)
			return true;
		else if(src_ip > cs.src_ip)
			return false;
		if(dst_ip < cs.dst_ip)
			return true;
		else if(dst_ip > cs.dst_ip)
			return false;
		if(dst_port < cs.dst_port)
			return true;
		else if(dst_port > cs.dst_port)
			return false;
		else if (protocol < cs.protocol)
			return true;
		else if(protocol > cs.protocol)
			return false;
		return false;
	}
	bool operator == (const tcp_connection_selector cs)const
	{
		if ( (src_ip == cs.src_ip) && (dst_ip == cs.dst_ip) && (src_port == cs.src_port) && (dst_port == cs.dst_port) && (protocol == cs.protocol))
			return true;
		return false;
	}
};


struct tcp_connection_answer
{
	struct sfw_half_ans
	{
		quint32 ISN;
		quint32 fin_seq;
		bool	last_acked;
		sfw_half_ans():ISN(0),fin_seq(0xfffffffb),last_acked(false){}
	};
	sfw_half_ans	 	dir_ans;
	sfw_half_ans	 	rev_ans;
	tcp_states	 	tcp_state;
	time_t			 	hit_time;
	tcp_connection_answer():
		 dir_ans()
		,rev_ans()
		,tcp_state(CLOSED)
		,hit_time(time(0))
	{}
};

struct time_selector
{
		time_t last_time;
		tcp_connection_selector selector;
		time_selector(const tcp_connection_selector& t_selector )
			:last_time(time(0))
			,selector(t_selector){}
};
class con_track:public QThread
{
	Q_OBJECT
public:
	pcap_if_t *alldevs;
	int inum;
	pcap_if_t *d;
	static const int	expiration_period	= 60;
	static unsigned int const max_entry_count = 1000000;
	typedef std::pair<tcp_connection_selector,tcp_connection_answer *> tcpconnection_table_pair;
	QReadWriteLock rwl;
	std::map <tcp_connection_selector,tcp_connection_answer *>					tcp_table;
	std::queue<time_selector>													expire_queue;
public:
	con_track(void);
	~con_track(void);
	
	void run();
	inline bool insert_connection(const packet_fcn* packet)
	{
		QWriteLocker wl(&rwl);
		while(tcp_table.size() > max_entry_count )
		{
			return false;
		}
		tcp_connection_answer *answer = new tcp_connection_answer;
		answer->tcp_state = SYN_SENT;
		answer->dir_ans.ISN = packet->tcp_get_seq_num();

		tcp_connection_selector const selector(packet->ip_get_r_src(),packet->ip_get_r_dst(),packet->tcp_udp_get_r_src_port(),packet->tcp_udp_get_r_dst_port(),packet->ip_get_protocol());

		std::pair<map <tcp_connection_selector,tcp_connection_answer*>::iterator,bool> ret_pair;
		tcpconnection_table_pair entry_pair(selector,answer);
		ret_pair = tcp_table.insert(entry_pair);
		expire_queue.push(selector);

		if(ret_pair.second == false)
		{
			ret_pair.first->second = answer;
		}
		return true;
	}

	void d_remove_restarted_connection(const packet_fcn* packet);
	void r_remove_restarted_connection(const packet_fcn* packet);
	tcp_connection_answer* get_answer(packet_fcn const* packet , direction & dir , bool &res );
	void process ( packet_fcn  *  packet ) ;
	tcp_states core(direction dir , pkt_type pkt_type , tcp_states current_state);
	void do_tcp_protection(packet_fcn * const packet,tcp_connection_answer * const sfw_answer,direction dir);
	pkt_type packet_type( packet_fcn const * packet,bool& is_ack);
	void ack_process(packet_fcn * const packet , tcp_connection_answer * const sfw_answer  ,direction dir);
	void do_expiration();
	signals:
		void on_del(tcp_connection_selector sel);
};
