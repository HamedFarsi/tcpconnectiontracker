#include "con_track.h"
#include <QReadLocker>
#include <QWriteLocker>
#include<Winsock2.h>

con_track::con_track(void)
{
	alldevs = NULL;
	pcap_if_t *d;
	inum=0;
	int i=0;
	pcap_t *adhandle;
	int res;
	char errbuf[PCAP_ERRBUF_SIZE];
	struct tm *ltime;
	char timestr[16];
	struct pcap_pkthdr *header;
	const u_char *pkt_data;
	time_t local_tv_sec;

	
	if (pcap_findalldevs_ex(PCAP_SRC_IF_STRING, NULL, &alldevs, errbuf) == -1)
	{
		fprintf(stderr,"Error in pcap_findalldevs: %s\n", errbuf);
		exit(1);
	}

}

con_track::~con_track(void)
{
}


void con_track::d_remove_restarted_connection( const packet_fcn* packet )
{
	QWriteLocker wl(&rwl);
	if(tcp_table.empty())
		return;
	tcp_connection_selector const d_selector(packet->ip_get_r_src(),packet->ip_get_r_dst(),packet->tcp_udp_get_r_src_port(), packet->tcp_udp_get_r_dst_port(),packet->ip_get_protocol());
	std::map <tcp_connection_selector,tcp_connection_answer *>::iterator iter;
	iter = tcp_table.find(d_selector);
	if(iter != tcp_table.end())
	{
		emit on_del(d_selector);
		tcp_table.erase(d_selector);
		
	}
}

void con_track::r_remove_restarted_connection( const packet_fcn* packet )
{
	QWriteLocker wl(&rwl);
	if(tcp_table.empty())
		return;
	tcp_connection_selector const r_selector(packet->ip_get_r_dst(),packet->ip_get_r_src(), packet->tcp_udp_get_r_dst_port(),packet->tcp_udp_get_r_src_port(),packet->ip_get_protocol());
	std::map <tcp_connection_selector,tcp_connection_answer *>::iterator iter;
	iter = tcp_table.find(r_selector);
	if(iter != tcp_table.end())
	{
		emit on_del(r_selector);
		tcp_table.erase(r_selector);
		
	}
}
tcp_connection_answer * con_track::get_answer( packet_fcn const* packet , direction & dir , bool &res  )
{
	tcp_connection_selector const d_selector(packet->ip_get_r_src(),packet->ip_get_r_dst(),packet->tcp_udp_get_r_src_port(), packet->tcp_udp_get_r_dst_port(),packet->ip_get_protocol());
	QReadLocker rl(&rwl);
	if(tcp_table.empty())
	{
		res = false;
		return NULL;
	}
	dir = UNKNOWN_DIRECTION;
	std::map <tcp_connection_selector,tcp_connection_answer *>::const_iterator itr = tcp_table.find(d_selector);
	if (itr != tcp_table.end())
	{
		dir = DIR;
		return itr->second;
	}
	else
	{
		tcp_connection_selector const r_selector(packet->ip_get_r_dst(),packet->ip_get_r_src(), packet->tcp_udp_get_r_dst_port(),packet->tcp_udp_get_r_src_port(),packet->ip_get_protocol());
		itr = tcp_table.find(r_selector);
		if(itr != tcp_table.end())
		{
			dir =  REV;
			return itr->second;
		}
	}
	res = false;
	return NULL;
}

void con_track::process ( packet_fcn  *  packet )  
{ 
	if(packet->eth_get_r_type() != ETH_TYPE_R_IPV4)
		return;

	const int protocol = packet->ip_get_protocol();
	if(protocol != IPPROTO_TCP)
		return;
	do_expiration();
							
	direction dir = UNKNOWN_DIRECTION;
	bool res = true;
	tcp_connection_answer* ans = get_answer(packet,dir,res);

		if(res)
		{
			do_tcp_protection(packet,ans,dir);
			ans->hit_time = time(0);
			return;
		}
		else // there is not any answer
		{
			bool is_ack=false;
			const pkt_type pkt_type = packet_type(packet,is_ack);
			if ( pkt_type == SYN ) // creation of a healthy connection.
				insert_connection(packet);
		}
}

pkt_type con_track::packet_type( packet_fcn const * packet,bool& is_ack)
{
	quint8 tcp_flags = packet->tcp_get_flags();
	tcp_flags = tcp_flags & 0x17 ; // 6 valid bits and URG and PSH are forced to be 0.

	if((tcp_flags & 0x10) == 0x10)
		is_ack = true; // ack bit is set , maybe another flags are set  or not

	if((tcp_flags & 0x04) == 0x04)
		return RST; //rst bit fucks another bits

	tcp_flags = tcp_flags & 0x07 ; //is_ack is recognized so we zero it
	switch(tcp_flags)
	{
	case 0x02:
		{
			if(is_ack)
				return SYNACK;
			return SYN;
		}
	case 0x01:
		return FIN;
	case 0x00:
		return ACK;//only ack bit is set and another bits are zero
	default:
		    return UNKNOWN ;
	}
}
void con_track::ack_process(packet_fcn * const packet , tcp_connection_answer * const sfw_answer  ,direction dir)
{
	const quint32 ack_num = packet->tcp_get_ack_num();
	if(dir == DIR)
	{
		if( ack_num == sfw_answer->rev_ans.ISN + 1 )// probably ack3
		{
			switch(sfw_answer->tcp_state)
			{
			case SYN_RCV: // ack3
				{
					sfw_answer->tcp_state = ESTABLISHED;
					return;
				}
			case ESTABLISHED:case FINISHING:// only ack of data //***change***
				{
					return;
				}
			default:
					return;
			}
		}
		else if( ack_num == sfw_answer->rev_ans.fin_seq + 1 )//last FIN_ACK(ack of second fin)
		{
			sfw_answer->dir_ans.last_acked = true;
			return;
		}
		else//only data acks 
		{
			return;
		}
	}
	else
	{
		if( ack_num == sfw_answer->dir_ans.fin_seq + 1 )//last fin ack
		{
			sfw_answer->rev_ans.last_acked = true;
			return;
		}
		else//data acks
		{
			return;
		}
		
	}
}
void con_track::do_tcp_protection(packet_fcn * const packet,tcp_connection_answer * const sfw_answer,direction dir)
{
	bool is_ack = false;
	const pkt_type pkt_type = packet_type(packet,is_ack);

	if(dir == UNKNOWN_DIRECTION)
	{
		return;
	}
	else
	{
		if(pkt_type == UNKNOWN )
		{
			return;
		}
		else if(pkt_type == RST) // strict action. no need for look up table.
		{
			if(dir == DIR)
				d_remove_restarted_connection(packet);
			else if(dir == REV)
				r_remove_restarted_connection(packet);
		}
		else if(pkt_type != ACK )  // SYN, SYNACK, FIN
		{
			sfw_answer->tcp_state = core(dir,pkt_type,sfw_answer->tcp_state);

			if (pkt_type == FIN)
			{
				if (dir == DIR)
				{
					sfw_answer->dir_ans.fin_seq = packet->tcp_get_seq_num();
				}
				else if( dir == REV )
				{
					sfw_answer->rev_ans.fin_seq = packet->tcp_get_seq_num();
				}
			}
			else if(pkt_type == SYNACK)
			{
				if(dir == REV)
					sfw_answer->rev_ans.ISN = packet->tcp_get_seq_num();
			}
			else
			{
				;
			}
		}
		else //if(is_ack == true && pkt_type != SYNACK)
			ack_process(packet,sfw_answer,dir);

		if( sfw_answer->dir_ans.last_acked && sfw_answer->rev_ans.last_acked)
			sfw_answer->tcp_state = TIME_WAIT;
	}

}
tcp_states con_track::core( direction dir , pkt_type pkt_type , tcp_states current_state )
{
	if(dir == DIR)
	{
		if(pkt_type == SYN)
		{
			if(current_state == CLOSED)
			{
				return SYN_SENT;
			}
			if(current_state == SYN_SENT)
			{
				return SYN_SENT;
			}
			if(current_state == SYN_RCV)
			{
				return SYN_RCV;
			}
			if(current_state == ESTABLISHED)
			{
				return ESTABLISHED;
			}
			if(current_state == FINISHING)
			{
			}
			if(current_state == TIME_WAIT)
			{
				return TIME_WAIT;
			}


		}
		else if(pkt_type == SYNACK)
		{
			if(current_state == CLOSED)
			{
				return CLOSED;
			}
			if(current_state == SYN_SENT)
			{
				return SYN_SENT;
			}
			if(current_state == SYN_RCV)
			{
				return SYN_RCV;
			}
			if(current_state == ESTABLISHED)
			{
				return ESTABLISHED;
			}
			if(current_state == FINISHING)
			{
				return FINISHING;  
			}
			if(current_state == TIME_WAIT)
			{
				return TIME_WAIT;
			}
		}
		else if(pkt_type == FIN )
		{
			if(current_state == CLOSED)
			{
				return CLOSED;
			}
			if(current_state == SYN_SENT)
			{
				return SYN_SENT;
			}
			if(current_state == SYN_RCV)
			{
				return SYN_RCV;
			}
			if(current_state == ESTABLISHED)
			{
				return FINISHING;
			}
			if(current_state == FINISHING)
			{
				return FINISHING;
			}
			if(current_state == TIME_WAIT)
			{
				return TIME_WAIT;
			}
		}

	}
	else if(REV)
	{
		if(pkt_type == SYN)
		{
			if(current_state == CLOSED)
			{
				return SYN_SENT;
			}
			if(current_state == SYN_SENT)
			{
				return SYN_SENT;
			}
			if(current_state == SYN_RCV)
			{
				return SYN_RCV;
			}
			if(current_state == ESTABLISHED)
			{
				return ESTABLISHED;
			}
			if(current_state == FINISHING)
			{
				return FINISHING;
			}
			if(current_state == TIME_WAIT)
			{
				return TIME_WAIT;
			}

		}
		else if(pkt_type == SYNACK)
		{
			if(current_state == CLOSED)
			{
				return CLOSED;
			}
			if(current_state == SYN_SENT)
			{
				return SYN_RCV;
			}
			if(current_state == SYN_RCV)
			{
				return SYN_RCV;
			}
			if(current_state == ESTABLISHED)
			{
				return ESTABLISHED;
			}
			if(current_state == FINISHING)
			{
				return FINISHING;
			}
			if(current_state == TIME_WAIT)
			{
				return TIME_WAIT;
			}
		}
		else if(pkt_type == FIN )
		{
			if(current_state == CLOSED)
			{
				return CLOSED;
			}
			if(current_state == SYN_SENT)
			{
				return SYN_SENT;
			}
			if(current_state == SYN_RCV)
			{
				return SYN_RCV;
			}
			if(current_state == ESTABLISHED)
			{
				return FINISHING;
			}
			if(current_state == FINISHING)
			{
				return FINISHING;
			}
			if(current_state == TIME_WAIT)
			{
				return TIME_WAIT;
			}
		}
	}

	return CLOSED;
}

void con_track::do_expiration()
 {
	
	QWriteLocker wl (&rwl);
	if(expire_queue.empty())
		return;
	else
	{
		if((time(NULL) - expire_queue.front().last_time) < expiration_period )
			return;
	}
	while ( true )
	{
		if ( expire_queue.empty() )
			return ;
		const time_selector & queue_front = expire_queue.front();
		if( (time(0) - queue_front.last_time) < expiration_period )
			return ;
		std::map <tcp_connection_selector,tcp_connection_answer *>::const_iterator itr = tcp_table.find(queue_front.selector);
		if(itr != tcp_table.end())
		{
			if( ( time (0) -  itr->second->hit_time) < expiration_period ) // update queue
			{
				time_selector time_sel(queue_front.selector);
				expire_queue.pop();
				expire_queue.push(time_sel);
				continue;
			}
			emit on_del(itr->first);
			tcp_table.erase(queue_front.selector);
			
			expire_queue.pop();
		}
		else
			expire_queue.pop();
	}
}

void con_track::run()
{

	
	int i=0;
	pcap_t *adhandle;
	int res;
	char errbuf[PCAP_ERRBUF_SIZE];
	struct tm *ltime;
	char timestr[16];
	struct pcap_pkthdr *header;
	const u_char *pkt_data;
	time_t local_tv_sec;
	packet_fcn packet;



	for(d=alldevs, i=0; i< inum-1 ;d=d->next, i++);


	if ( (adhandle= pcap_open(d->name,         
		65536,           
		
		PCAP_OPENFLAG_PROMISCUOUS,    
		1000,             
		NULL,             
		errbuf            
		) ) == NULL)
	{
		pcap_freealldevs(alldevs);
		return;
	}


	pcap_freealldevs(alldevs);
 
	while((res = pcap_next_ex( adhandle, &header, &packet.pkt)) >= 0){

		if(res == 0)
			continue;
		process(&packet);

		local_tv_sec = header->ts.tv_sec;
		ltime=localtime(&local_tv_sec);
		strftime( timestr, sizeof timestr, "%H:%M:%S", ltime);
	}

	if(res == -1){
		return;
	}
}

