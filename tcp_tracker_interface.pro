#-------------------------------------------------
#
# Project created by QtCreator 2014-08-02T16:48:48
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = tcp_tracker_interface
TEMPLATE = app


win32:CONFIG(release, debug|release): LIBS += -L$$PWD/ -lwpcap
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/ -lwpcap
else:unix: LIBS += -L$$PWD/ -lwpcap

INCLUDEPATH += $$PWD/
INCLUDEPATH += ./Include
DEPENDPATH += $$PWD/


SOURCES += main.cpp\
        tcp_tracker_interface.cpp \
        con_track.cpp \
        packet_fcn.cpp


HEADERS  += tcp_tracker_interface.h \
            con_track.h \
            defs.h \
            packet_fcn.h \
    Include/pcap/bluetooth.h \
    Include/pcap/bpf.h \
    Include/pcap/namedb.h \
    Include/pcap/pcap.h \
    Include/pcap/sll.h \
    Include/pcap/usb.h \
    Include/pcap/vlan.h \
    Include/bittypes.h \
    Include/ip6_misc.h \
    Include/Packet32.h \
    Include/pcap-bpf.h \
    Include/pcap-namedb.h \
    Include/pcap-stdinc.h \
    Include/pcap.h \
    Include/remote-ext.h \
    Include/Win32-Extensions.h

FORMS    += tcp_tracker_interface.ui

RESOURCES += \
    resources/res.qrc

