#include "tcp_tracker_interface.h"
#include "ui_tcp_tracker_interface.h"
#include <QHostAddress>

tcp_tracker_interface::tcp_tracker_interface(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::tcp_tracker_interface)
{
    ui->setupUi(this);
	
	pcap_if_t *d=_track.alldevs;
	for(; d; d=d->next)
	{
		ui->cmb_interface->addItem(QIcon("qrc:/interface.png"),QString(d->description));
	}

	_timer.setInterval(500);
	 qRegisterMetaType<tcp_connection_selector>("tcp_connection_selector");
	connect(&_timer,SIGNAL(timeout()),this,SLOT(refresh_table()));
	connect(&_track,SIGNAL(on_del(tcp_connection_selector)),this,SLOT(del_exp(tcp_connection_selector)));
}

tcp_tracker_interface::~tcp_tracker_interface()
{
    delete ui;
}

void tcp_tracker_interface::on_btn_track_clicked()
{
		_track.inum = ui->cmb_interface->currentIndex()+1;
		_track.start();
		_timer.start(500);
}

void tcp_tracker_interface::refresh_table()
{
	QReadLocker mtx(&_track.rwl);
	std::map<tcp_connection_selector,tcp_connection_answer*>::iterator itr;

	for(itr = _track.tcp_table.begin(); itr !=_track.tcp_table.end() ; itr++)
	{
		tcp_connection_selector selec;
		selec.src_ip = ZM_uint32_reverse(itr->first.src_ip);
		selec.dst_ip = ZM_uint32_reverse(itr->first.dst_ip);
		selec.src_port = ZM_uint16_reverse(itr->first.src_port);
		selec.dst_port = ZM_uint16_reverse(itr->first.dst_port);
		selec.protocol = IPPROTO_TCP;
		QStringList lst_data ;
		lst_data	<< QHostAddress(selec.src_ip).toString()	<< QHostAddress(selec.dst_ip).toString()
					<< QString::number(selec.src_port)			<< QString::number(selec.dst_port)
					<< QString("TCP") << tcp_states_toString(itr->second->tcp_state);
		int recored_id = find_row_number(lst_data);
		if (recored_id > -1)
		{
			ui->tblwid->item(recored_id,5)->setText(lst_data[5]);
			set_color(lst_data[5], recored_id);
			continue;
		}

		ui->tblwid->insertRow(ui->tblwid->rowCount());
		QTableWidgetItem* item_src_ip = new QTableWidgetItem(QIcon("qrc:/ip.png"),lst_data[0]);
		QTableWidgetItem* item_dst_ip = new QTableWidgetItem(QIcon("qrc:/ip.png"),lst_data[1]);
		QTableWidgetItem* item_src_port = new QTableWidgetItem(QIcon("qrc:/port.png"),lst_data[2]);
		QTableWidgetItem* item_dst_port = new QTableWidgetItem(QIcon("qrc:/port.png"),lst_data[3]);
		QTableWidgetItem* item_state = new QTableWidgetItem(QIcon("qrc:/state.png"),lst_data[5]);
		QTableWidgetItem* item_protocol = new QTableWidgetItem(QIcon("qrc:/protocol.png"),lst_data[4]);
		ui->tblwid->setItem(ui->tblwid->rowCount()-1, 0 , item_src_ip);
		ui->tblwid->setItem(ui->tblwid->rowCount()-1, 1 , item_dst_ip);
		ui->tblwid->setItem(ui->tblwid->rowCount()-1, 2 , item_src_port);
		ui->tblwid->setItem(ui->tblwid->rowCount()-1, 3 , item_dst_port);
		ui->tblwid->setItem(ui->tblwid->rowCount()-1, 4 , item_protocol);
		ui->tblwid->setItem(ui->tblwid->rowCount()-1, 5 , item_state);
		
		set_color(lst_data[5], ui->tblwid->rowCount()-1);
	}
}

int tcp_tracker_interface::find_row_number(QStringList lst)
{
	if(lst.size() < 5)
		return -1;
	int row_count = ui->tblwid->rowCount();
	
	for (int i=0 ; i <row_count ; i++)
	{
		
		QString tmp1=ui->tblwid->item(i,0)->text();
		QString tmp2=ui->tblwid->item(i,1)->text();
		QString tmp3=ui->tblwid->item(i,2)->text();
		QString tmp4=ui->tblwid->item(i,3)->text();
		QString tmp5=ui->tblwid->item(i,4)->text();

		if ( ui->tblwid->item(i,0)->text()==lst[0] && //source ip
			 ui->tblwid->item(i,1)->text()==lst[1] && //destination ip
			 ui->tblwid->item(i,2)->text()==lst[2] && //source port
			 ui->tblwid->item(i,3)->text()==lst[3] && //destinatin port
			 ui->tblwid->item(i,4)->text()==lst[4]    //protocol
			)
		{
			return i ;
		}
	}

	return -1;
}

void tcp_tracker_interface::set_color( QString str_state, int recored_id )
{
	if (str_state == "SYN_SENT")
	{
		ui->tblwid->item(recored_id,5)->setBackground(QBrush(QColor(254,67,71)));
	}
	if (str_state == "SYN_RCV")
	{
		ui->tblwid->item(recored_id,5)->setBackground(QBrush(QColor(237,165,84)));
	}
	if (str_state == "ESTABLISHED")
	{
		ui->tblwid->item(recored_id,5)->setBackground(QBrush(QColor(98,231,90)));
	}
	if (str_state == "FINISHING")
	{
		ui->tblwid->item(recored_id,5)->setBackground(QBrush(QColor(243,13,1)));
	}
	if (str_state == "TIME_WAIT")
	{
		ui->tblwid->item(recored_id,5)->setBackground(QBrush(QColor(221,129,4)));
	}

}

void tcp_tracker_interface::del_exp( tcp_connection_selector selec )
{
 	QStringList lst_data ;
	lst_data	<< QHostAddress(ZM_uint32_reverse(selec.src_ip)).toString()	<< QHostAddress(ZM_uint32_reverse(selec.dst_ip)).toString()
		<< QString::number(ZM_uint16_reverse(selec.src_port))			<< QString::number(ZM_uint16_reverse(selec.dst_port))
		<< QString("TCP");

	int recored_id = find_row_number(lst_data);
	if (recored_id > -1)
	{
		ui->tblwid->removeRow(recored_id);
	}
}
